# Pezesha Banking API Mini Porject (Bootsrap frontend)


This is my submission for the front end build on Bootstrap  for the banking API. These pages serve to provide an aggregated summary of transactions on the account. As well as the tools to modify limits that might be imposed on the account.

One might need to modify the backend server address for the js scripts. This would need to be done the 3 scripts that server the front end. These are located in the js/pezesha directory.

```javascript
 var baseurl = "http://localhost:8080";
```
 


