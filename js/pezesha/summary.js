$(document).ready(function () {
	var baseurl = "http://localhost:8080";
	$.ajax({
		url: baseurl + "/summary"
	}).then(function (data) {
		var TotalWithdrawals = data.totalWithdrawals;
		var TotalDeposits = data.totalDeposits;
		var TodayWithdrawals = data.todayWithdrawals;
		var TodayDeposits = data.todayDeposits;

		Morris.Donut({
			element: 'totalTrans',
			data: [{
					value: TotalDeposits,
					label: 'Withdrawals'
				},
				{
					value: TotalWithdrawals,
					label: 'Deposits'
				}
			],
			backgroundColor: '#ccc',
			labelColor: '#fff',
			colors: [
				'#EC2458',
				'#39B580'
			],
			formatter: function (x) {
				return "$" + x
			}
		});
		Morris.Donut({
			element: 'todayTrans',
			data: [{
					value: TodayWithdrawals,
					label: 'Withdrawals'
				},
				{
					value: TodayDeposits,
					label: 'Deposits'
				}
			],
			backgroundColor: '#ccc',
			labelColor: '#fff',
			colors: [
				'#EC2458',
				'#3A6DDB'
			],
			formatter: function (x) {
				return "$" + x
			}
		});
	});
});
