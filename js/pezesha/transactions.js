$(document).ready(function () {
	var baseurl = "http://localhost:8080";
	$.ajax({
		url: baseurl + "/transactions"
	}).then(function (incomingData) {
		$('#transactions').DataTable({
			responsive: true,
			data: incomingData,
			columns: [{
					data: 'timestamp'
				},
				{
					data: 'credit'
				},
				{
					data: 'debit'
				},
				{
					data: 'balance'
				}
			]
		});
	});


});
