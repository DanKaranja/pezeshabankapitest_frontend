$(document).ready(function () {

	var baseurl = "http://localhost:8080";

	$.ajax({
			url: baseurl + "/getSettings"
		})
		.then(function (data) {
			$('#MAX_DAILY_TRANSACTION_CREDIT').val(data.MAX_DAILY_TRANSACTION_CREDIT);
			$('#MAX_TRANSACTION_CREDIT').val(data.MAX_TRANSACTION_CREDIT);
			$('#MAX_DAILY_DEPOSIT_COUNT').val(data.MAX_DAILY_DEPOSIT_COUNT);
			$('#MAX_DAILY_TRANSACTION_DEBIT').val(data.MAX_DAILY_TRANSACTION_DEBIT);
			$('#MAX_TRANSACTION_DEBIT').val(data.MAX_TRANSACTION_DEBIT);
			$('#MAX_DAILY_WITHDRAW_COUNT').val(data.MAX_DAILY_WITHDRAW_COUNT);
		});

	$("#settingsForm").submit(function (event) {
		event.preventDefault();
		var Data = {
			"MAX_DAILY_TRANSACTION_CREDIT": $('#MAX_DAILY_TRANSACTION_CREDIT').val(),
			"MAX_TRANSACTION_CREDIT": $('#MAX_TRANSACTION_CREDIT').val(),
			"MAX_DAILY_DEPOSIT_COUNT": $('#MAX_DAILY_DEPOSIT_COUNT').val(),
			"MAX_DAILY_TRANSACTION_DEBIT": $('#MAX_DAILY_TRANSACTION_DEBIT').val(),
			"MAX_TRANSACTION_DEBIT": $('#MAX_TRANSACTION_DEBIT').val(),
			"MAX_DAILY_WITHDRAW_COUNT": $('#MAX_DAILY_WITHDRAW_COUNT').val(),

		}
		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: baseurl + "/setSettings",
			data: JSON.stringify(Data),
			dataType: 'json',
			timeout: 100000,
			success: function (data) {
				console.log("SUCCESS: ", data);
			},
			error: function (e) {
				console.log("ERROR: ", e);
			},
			done: function (e) {
				console.log("DONE");
				location.reload();
			}
		});

	});

});
